local chat_id = minetest.settings:get("telechat_chat_id")
local token = minetest.settings:get("telechat_bot_token")
local http = minetest.request_http_api()

if chat_id == 0 or token == nil or http == nil then
  minetest.log("error", "Failed to load telechat!")
  return
end

local function char_to_hex(c)
  return string.format("%%%02X", string.byte(c))
end

local function encode_url(url)
  url = url:gsub("\n", "\r\n")
  url = url:gsub("([^%w ])", char_to_hex)
  url = url:gsub(" ", "+")
  return url
end

minetest.register_on_chat_message(function(name, message)
  local url = "https://api.telegram.org/bot" .. token ..
              "/sendMessage?chat_id=" .. chat_id ..
              "&text=" .. encode_url("<" .. name .. "> "..  message)
  http.fetch_async({url = url})
end)

